<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Cache-Control, Pragma, If-Modified-Since, X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Max-Age');


// Require the db class file
   require("functions/ToolDB.class.php");
	
// Instantiate the db classes
   $orders  	= 	new Orders();
   $users		=	new Users();

if ( //List orders
	isset($_GET['orders'])
	&& !empty($_GET['orders'])
) {
	
	$orders = $orders->all();  
	
	echo json_encode($orders);
	
} else if ( //Get order
	isset($_GET['singleorder'])
	&& !empty($_GET['singleorder'])
) {
	
	$orders->id = $_GET['singleorder'];		
	$orders->Find();
	
	$orderget = array(
	'kenteken' => $orders->kenteken,
	'reparateur' => $orders->reparateur,	
	'opmerking' => $orders->opmerking,
	'img' => $orders->img,
	'checklist' => $orders->checklist,
	'lat' => $orders->lat,
	'lon' => $orders->lon	
	);
	
	echo json_encode($orderget);
	
} else if ( //Get order
	isset($_GET['removeorder'])
	&& !empty($_GET['removeorder'])
) {
	
	$orders->id = $_GET['removeorder'];		
	$delete = $orders->Delete();
	
	echo json_encode("Order: ".$_GET['removeorder']." verwijderd");
	
} else if ( //Get User
	isset($_GET['user'])
	&& !empty($_GET['user'])
) {
	
	$users->naam = $_GET['user'];		
	$users->Find();
	
	$userget = array(
	'naam' => $users->naam,
	'garage' => $users->garage,	
	'api_url' => $users->api_url
	);
	
	echo json_encode($userget);
	
} else if ( //Basedecode
	isset($_GET['base'])
	&& !empty($_GET['base'])
) {

echo base64_decode($_GET["base"]);

} else if ( //Get KentekenData
	isset($_GET['kenteken'])
	&& !empty($_GET['kenteken'])
) {
	

	require_once('lib/nusoap.php');
	$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
	$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
	$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
	$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
	$useCURL = isset($_POST['usecurl']) ? $_POST['usecurl'] : '0';
	$client = new nusoap_client("http://services.satorholding.com/nhs/kenteken/Info.asmx?WSDL", true, $proxyhost, $proxyport, $proxyusername, $proxypassword);
		$err = $client->getError();
		if ($err) {
			echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
			echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
			exit();
		}
		$client->setUseCurl($useCURL);
		$client->soap_defencoding = 'UTF-8';

		$params = array(
			'Kenteken' => ''.$_GET['kenteken'].''
		);		
					
		$kentekenwsdl = $client->call("KentekenDetailsHtml", $params, "KentekenDetailsHtmlResponse", "http://services.satorholding.com/nhs/kenteken/Info.asmx/KentekenDetailsHtml");

		$style = "<style> \n.title\n{\n\tpadding: 10px 0px 2px 0px !important;\n}\n.voertuigGegevens *\n{\n\tborder: none !important;\n}\n.voertuigGegevens th\n{\n\tpadding: 0px 0px 0px 25px !important;\n}\n\n.voertuigGegevens td\n{\n\tpadding: 0px !important;\n}\n.title, .subtitle\n{\n\tfont-weight: bold; \n\tfont-size: 11px; \n\tcolor: #cc3300;\n\tmargin-left: 0;\n\ttext-align: left;\n}\n#panels div\n{\n\tdisplay: block;\n\tpadding: 10px;\n\tborder: 1px solid #cc3300;\n}\t\n#main-content\n{\t\n\tpadding-left:10px;\n\tpadding-right:10px;\n}\n#content-container #main-content table\n{\n\tpadding:2px;\n\tborder-collapse:collapse;\n\tbackground-color: #FFFFFF;\n}\n#content-container #main-content table td,\n#content-container #main-content table th\n{\n\tborder:1px solid #72797f;\n\tpadding:5px;\n\ttext-align:left;\n\tvertical-align:top;\n}\n* {\n\tfont-family:Verdana, sans-serif;\n\tfont-size:10px;\n\tcolor:#000000;\n\tline-height:14px;\n\t\n}\n#main-container\n{\t\nwidth:410px;\n\tpadding:0px;\n}\n<\/style>";
		
		
		
		$kent = $kentekenwsdl['KentekenDetailsHtmlResult'];	
		
		$kenteken = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $kent);
		
		echo $kenteken;
		
		
} else if ( //Write order
	isset($_POST['slug']) //Check if ajax does post and if it is orders
	&& isset($_POST) 
	&& !empty($_POST['slug']) 
	&& filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest'
){ 

	if ($_POST['slug']  == 'orders'){
	// Create order
	
	
	$orders->datum	=	date('Y-m-d H:i:s');
	   $orders->kenteken 			= $_POST['kenteken'];
	   $orders->reparateur		  	= $_POST['reparateur'];
	   $orders->opmerking		  	= $_POST['opmerking'];
	   $orders->img	  				= $_POST['img'];  
	   $orders->checklist			= $_POST['checklist'];
	$orders->lat 	= $_POST['lat'];
	$orders->lon 	= $_POST['lon'];
	$orders->by_userid	= '1';

	   $creation = $orders->Create(); //Insert into db mattie G
	   
	   if($creation) 
			echo json_encode("SQL geschreven\n Kenteken:".$orders->kenteken."\n \n Checklist: ".$orders->checklist." \n Reparateur:".$orders->reparateur);

	   else
			echo json_encode("SQL schrijven mislukt");
	
	} else 	if ($_POST['slug']  == 'ordersupdate'){
	// Create order
	
	
	   $orders->id					= $_POST['id'];
	$orders->datum	=	date('Y-m-d H:i:s');
	   $orders->kenteken 			= $_POST['kenteken'];
	   $orders->reparateur		  	= $_POST['reparateur'];
	   $orders->opmerking		  	= $_POST['opmerking'];
	   $orders->img	  				= $_POST['img'];  
	   $orders->checklist			= $_POST['checklist'];
	$orders->by_userid	= '1';

	   $creation = $orders->Save(); //Insert into db mattie G
	   
	   if($creation) 
			echo json_encode("SQL update\n Kenteken:".$orders->kenteken."\n \n Checklist: ".$orders->checklist." \n Reparateur:".$orders->reparateur);

	   else
			echo json_encode("SQL schrijven mislukt");
	
	} else 	if ($_POST['slug']  == 'base'){
	// Create pdf
	


		$data = $_POST['base'];
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
		//Write data back to pdf file
		$pdf = fopen ('tmp/'.$_POST['title'].'.pdf','w');
		fwrite ($pdf,$data);
		//close output file
		fclose ($pdf);



		echo 'http://'.$_SERVER['HTTP_HOST'].'/phonetool/tmp/'.$_POST['title'].'.pdf';


	}
	

	
}else {}


?>
