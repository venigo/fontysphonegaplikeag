-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Gegenereerd op: 14 jun 2015 om 15:42
-- Serverversie: 5.5.11
-- PHP-versie: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `dbi281875`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`id` int(11) NOT NULL,
  `kenteken` varchar(10) NOT NULL,
  `reparateur` varchar(20) NOT NULL,
  `opmerking` text NOT NULL,
  `img` longtext NOT NULL,
  `datum` datetime NOT NULL,
  `by_userid` int(11) NOT NULL,
  `checklist` text NOT NULL,
  `lat` text NOT NULL,
  `lon` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Gegevens worden geëxporteerd voor tabel `orders`
--

INSERT INTO `orders` (`id`, `kenteken`, `reparateur`, `opmerking`, `img`, `datum`, `by_userid`, `checklist`, `lat`, `lon`) VALUES
(4, '41-jr-fn', 'Paul', 'Test', 'x-wmapp0:www/./img/unknown.png', '2015-06-11 12:14:23', 1, ',accu,test,filters', '', ''),
(5, '72-rr-nh', 'Paul', 'Test', 'x-wmapp0:www/./img/unknown.png', '2015-06-11 13:46:26', 1, ',accu,remmen', '', ''),
(28, '17-DS-VL', 'Paul', 'Ddddd', 'x-wmapp0:www/./img/unknown.png', '2015-06-11 16:39:16', 1, ',accu', '', ''),
(29, '84-SV-GZ', 'Paul', 'Test', 'http://127.0.0.1/fontysphonegaplikeag/Automotive%20Workshop/www/img/unknown.png', '2015-06-12 11:33:17', 1, '', '', ''),
(30, '55-nn-bh', 'Paul', 'Fff', 'x-wmapp0:www/./img/unknown.png', '2015-06-13 14:21:57', 1, ',ffdff,ffdff', '', ''),
(34, 'xccc', 'Paul', 'Xxxsbdshs', 'x-wmapp0:www/./img/unknown.png', '2015-06-14 14:34:51', 1, '', '51.54578038461538', '5.061240384615384');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `naam` varchar(100) NOT NULL,
  `garage` varchar(100) NOT NULL,
  `api_url` text NOT NULL,
  `location` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `naam`, `garage`, `api_url`, `location`) VALUES
(1, 'Paul', 'Broekhuizen', 'http://athena.fhict.nl/users/i281875/MIA/backend/data.php', 'Rachelsmolen 1, Eindhoven');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `kenteken` (`kenteken`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `orders`
--
ALTER TABLE `orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
