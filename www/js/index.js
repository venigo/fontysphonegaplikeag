
// Set vars and standard stuff
var ToolApp = ons.bootstrap("ToolApp", ["onsen", 'LocalStorageModule', 'mwl.calendar']);
var pictureSource;   // picture source
var destinationType; // sets the format of returned value 
var imgurl = "notset";
var offline = false;
var lat = 0; //set location vars
var lon = 0;

var params = {orderkey:null, user:null}; //Set orderkey params
params.user = "Paul"; //Set user..
var options;
var get;
var parse;
		
var srcimg;

var orderCount; //Set orderCount




// We'll make our own renderer to skip this editor
var specialElementHandlers = {
	'#editor': function(element, renderer){
		return true;
	}
};


//PDF base64 to blob
function b64toBlob(dataURI) {

    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);

    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: 'application/pdf' });
}

//Set config and prefixes
ToolApp.config(function (localStorageServiceProvider, $compileProvider, $locationProvider, $httpProvider) {

	//This is need because of the x-wmapp bug...
	$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|http|file|ghttps?|ms-appx|x-wmapp0):/);
	// Use $compileProvider.urlSanitizationWhitelist(...) for Angular 1.2
	$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|http|ms-appx|x-wmapp0):|data:image\//);
	$locationProvider.html5Mode(false);
	//Set localservice prefix
	localStorageServiceProvider.setPrefix('ToolApp'); 
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.headers.post['Accept'] = 'application/json, text/javascript';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
    $httpProvider.defaults.headers.post['Access-Control-Max-Age'] = '1728000';
    $httpProvider.defaults.headers.common['Access-Control-Max-Age'] = '1728000';
    $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
    $httpProvider.defaults.useXDomain = true;

	    if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.common = {};
        }
        $httpProvider.defaults.headers.common["Cache-Control"] = "no-cache";
        $httpProvider.defaults.headers.common.Pragma = "no-cache";
        $httpProvider.defaults.headers.common["If-Modified-Since"] = "0";
	
});

//Start the app build

ToolApp.controller('AppController', function($rootScope, $scope, localStorageService, $http, transformRequestAsFormPost) {
	

	
	onDeviceReady = function() { //Handle the event listener
		//Set camera plugin
		pictureSource = navigator.camera.PictureSourceType;
		destinationType = navigator.camera.DestinationType;
		navigator.geolocation.getCurrentPosition(onSuccess, onError);
		
		//Shop type storage debug
		//alert("typeof localStorage=" + typeof window.localStorage);
	}
	
	var onSuccess = function(position) {
		lat = ''+ position.coords.latitude+ '';
		lon = ''+position.coords.longitude+'';
	};

	// onError Callback receives a PositionError object
	//
	var onError = function(error) {
		alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
	};
	
	onOffline = function() { //Offline function
		offline = true;
	}
	
	if(offline == false)
		console.log("Device is online");
	else
		console.log("Device is offline");

	document.addEventListener("deviceready", onDeviceReady, false); //The device ready thing (for the camera)
	document.addEventListener("offline", onOffline, false); //Check if the device is online
	
	if(localStorageService.isSupported)
		console.log('storage supported: yes; type: local storage'); //Check if the localstorage is supported
  
	$rootScope.printPDF = function(value){ //Set print function
	var doc = new jsPDF(); //Set print stuff
		doc.fromHTML($('#render').get(0), 15, 15, {
			'width': 170, 
			'elementHandlers': specialElementHandlers
		}); //Set up the html for pdf
		
		
		
		var pdfOutput = doc.output('datauristring'); //Create pdf base64
		
	
		$http({ //Get print pdf..
		   url:'http://i281875.iris.fhict.nl/phonetool/data.php',
		   method:"POST",
		   headers: {
			'X-Requested-With': 'XMLHttpRequest',
			'Content-Type': 'application/x-www-form-urlencoded'
		   },
		   transformRequest: transformRequestAsFormPost,
			data    : eval({ 
			'slug' : "base", 
			base : ""+encodeURI(pdfOutput)+"",
			title: ""+value+""
			}),  // pass in data as strings
			
			isArray: true,
			callback: ''
	  }).success(function(data) {
			console.log(encodeURI(data));
			//window.plugins.socialsharing.share(value, null, data, null);
			window.open("http://docs.google.com/viewer?url="+data, "_blank");
	  });



	}
  
	$scope.clearAll = function() { //Function to clear local storage (in settings)
		localStorageService.clearAll();
		alert ("Lokale gegevens verwijderd");
	};
	
	$rootScope.setParam = function (value) { //Set param, used for the order data retreive
		params.orderkey = value;
	};
	
	$rootScope.setAlertParam = function (value) { //Set param, used for the order data retreive
		params.orderkey = value;
		$rootScope.close(); 
	};
	
	showModal = function (action, event) {
		console.log(action);
		ons.notification.alert({
		  messageHTML: '<div><h5>Reparatie order:'+event.title+'</h5><br><ons-button modifier="small" class="btn-primary" style="margin-left: 5px;" ng-click="setAlertParam(\''+ event.id +'\');" onclick="menu.setMainPage(\'order-data.html\', {closeMenu: true});"><span class="fa fa-arrow-right"></span></ons-button><ons-button  style="margin-left: 5px;" ng-click="setAlertParam(\''+ event.id +'\');" onclick="menu.setMainPage(\'order-edit.html\', {closeMenu: true})" class="btn-warning" modifier="small"><span class="fa fa-pencil"></span></ons-button><ons-button ng-click="removeOrder('+ event.id +')" style="margin-left: 5px;" class="btn-danger" modifier="small"><span class="fa fa-times"></span></ons-button></div>',
		  // or messageHTML: '<div>Message in HTML</div>',
		  title: event.title,
		  buttonLabel: 'OK',
		  cancelable: true,
		  animation: 'default', // or 'none'
		  // modifier: 'optional-modifier'
		  callback: function() {
			// Alert button is closed!
				$('.alert-dialog').remove(); 
				$('.alert-dialog-mask').remove(); 
		  }
		});
    };
	
	
	$rootScope.close = function(element) {
		$('.alert-dialog').remove(); 
		$('.alert-dialog-mask').remove(); 
	};	
	
    $rootScope.eventClicked = function(event) {
		showModal('Clicked', event);
    };

    $rootScope.eventEdited = function(event) {
		showModal('Edited', event);
    };

    $rootScope.eventDeleted = function(event) {
		showModal('Deleted', event);
    };

    $rootScope.toggle = function($event, field, event) {
      $event.preventDefault();
      $event.stopPropagation();
      event[field] = !event[field];
    };	
	
	order = function (kenteken,opmerking,repa,img) { //This should not be removed... oops. This is so set the submit at the add page
		this.kenteken = kenteken;
		this.opmerking = opmerking;
		this.repa = repa;
		this.img = img;
	}
	
});

ToolApp.controller('HomeCtrl', function($compile, $rootScope ,$scope, localStorageService, $http) {

	removeOrder = function(id) { //Remove for home view
	
			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?removeorder='+ id +'',
			   method:"GET"
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else {
				  // if successful, bind success message to message and fill the list
				
				$('.alert-dialog').remove(); 
				$('.alert-dialog-mask').remove(); 
				
				ons.notification.alert({
					messageHTML: '<div>'+data+'</div>',
					// or messageHTML: '<div>Message in HTML</div>',
					title: event.title,
					buttonLabel: 'OK',
					cancelable: true,
					animation: 'default', // or 'none'
					// modifier: 'optional-modifier'
					callback: function() {
						menu.setMainPage('home.html', {closeMenu: true})
					}	
				});
			}
			});
	};
	
	if(offline == true){
		//Set up the counter
		orderCount = localStorageService.get('orderCount');  
		
		//console.log(orderCount); //Debug
		
		//Set var
		$rootScope.orderAantal = localStorageService.get('orderCount');

		//Watch the goddamn var!!
		$scope.$watch('variable', function (value) {
			if (value){
				//console.log(value);
			}
		});
	}
	
}).directive('listOrders', function ($http, $rootScope, $compile, localStorageService) {
    return {
        restrict: 'A',
        // NB: no isolated scope!!
        link: function (scope, element, attrs) {
            // observe changes in attribute - could also be scope.$watch
            attrs.$observe('listOrders', function (value) {
			//scope.$watch('listOrders', function (value) {
			if(offline == true) {
				// Handle the offline event
				
				if (value == '') {
					$rootScope.noOrder = 'Geen reparatie orders';
				} else {
					$rootScope.noOrder = '';
				}
				
				orderCount = value;  
				
				if(is.set(orderCount)){
					for (i=1;i<=orderCount;i++) {
						var number = parseInt(i) + 1;
						var order = jQuery.parseJSON(localStorageService.get("order_" + i));
						var orderkey = "order_" + i;
						
						//Make html
						var input = angular.element('<ons-list-item ng-click="setParam(\''+orderkey+'\')" onclick="menu.setMainPage(\'order-data.html\', {closeMenu: true})" class="list__item--tappable list__item__line-height" modifier="chevron">Reparatie '+ order.kenteken +'</ons-list-item>');

						// Append input to div
						$compile(input)(scope);
						element.append(input);
					}
				}
			} else {
				//Handle online

			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?orders=all',
			   method:"GET"
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else if(data == '') {
						$rootScope.noOrder = 'Geen reparatie orders';
				} else {
				  // if successful, bind success message to message and fill the list
				 $rootScope.noOrder = '';
					angular.forEach(data, function(data) {
						var input = angular.element('<div ng-click="setParam(\''+ data.id +'\')"  modifier="chevron"><div style="display: inline-flex; padding: 5px;"><input class="licenseplatebar" id="licenseplate" name="licenseplate" maxlength="8" value="'+ data.kenteken +'" disabled /><div class="licenseplatecar pull-right"><ons-button modifier="small" class="btn-primary" style="margin-left: 5px;" onclick="menu.setMainPage(\'order-data.html\', {closeMenu: true})"><span class="fa fa-arrow-right"></span></ons-button><ons-button  style="margin-left: 5px;" onclick="menu.setMainPage(\'order-edit.html\', {closeMenu: true})" class="btn-warning" modifier="small"><span class="fa fa-pencil"></span></ons-button><ons-button onclick="removeOrder('+ data.id +')" style="margin-left: 5px;" class="btn-danger" modifier="small"><span class="fa fa-times"></span></ons-button></div></div></div>');

							// Append input to div
						$compile(input)(scope);
						element.append(input);
					});
				 
				}
			  });

			}
			
            });
        }
    };
});


ToolApp.controller('CalendarCtrl', function($http, $compile, $rootScope ,$scope, localStorageService, moment) {
	moment.locale('nl');
     var currentYear = moment().year();
     var currentMonth = moment().month();
	 var calendarDay = new Date();
	 
	$rootScope.removeOrder = function(id) { //Remove for calendar view
	
			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?removeorder='+ id +'',
			   method:"GET"
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else {
				  // if successful, bind success message to message and fill the list
				
				$('.alert-dialog').remove(); 
				$('.alert-dialog-mask').remove(); 
				
				ons.notification.alert({
					messageHTML: '<div>'+data+'</div>',
					// or messageHTML: '<div>Message in HTML</div>',
					title: event.title,
					buttonLabel: 'OK',
					cancelable: true,
					animation: 'default', // or 'none'
					// modifier: 'optional-modifier'
					callback: function() {
						menu.setMainPage('calendar.html', {closeMenu: true});
					}	
				});
			}
			});
	};	 

    //These variables MUST be set as a minimum for the calendar to work
    $scope.calendarView = 'week';
    $scope.calendarDay = new Date();
    $scope.events = [];
	
	  		$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?orders=all',
			   method:"GET",
			   compress:true
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else if(data == '') {
						$rootScope.noOrder = 'Geen reparatie orders';
				} else {
				  // if successful, bind success message to message and fill the list
				 $rootScope.noOrder = '';
					angular.forEach(data, function(data) {
					$scope.events.push({id: data.id,title:data.kenteken,type: 'important', startsAt: data.datum, endsAt: moment().startOf('day').add(19, 'hours').toDate()});
							// Push to event array
					});
				}

	});


	
});

ToolApp.controller('FormCtrl', function($http, $rootScope, $scope, localStorageService, transformRequestAsFormPost ) {

	function getNum(val)
	{
	   if (isNaN(val)) 
		 return null;
	   else
		 return val;
	}

	$scope.removeItem = function($index) { //Functions to remove checklist item
		$scope.checklist.splice( $scope.checklist.indexOf(), $index )
		console.log($index);
	};
	
	$scope.checklist=['']; //Param
	$scope.addCheck = function($index){ //Add function
		$scope.checklist.push($scope.newCheck);
		console.log($scope.checklist);
	};

	if(offline == true) {
		// Handle the offline event

		function commitToStorage(objectCount,newObject) {
		  // The unique key of the object:
		  var item = 'order_' + objectCount;
		  localStorageService.set('orderCount', objectCount);
		  
		  // Put the object into storage
		  localStorageService.set(item, JSON.stringify(newObject));
		  
		  alert ("Reparatie order toegevoegd!");
		  $rootScope.orderAantal = localStorageService.get('orderCount');
		  // Create Markup
		}
	} else {
		//Handle online event
		

		

		
		
		function commitToStorage(objectCount,newObject) {
		
			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php',
			   method:"POST",
			   headers: {
				'X-Requested-With': 'XMLHttpRequest',
				'Content-Type': 'application/x-www-form-urlencoded'
			   },
			   transformRequest: transformRequestAsFormPost,
			    data    : eval({ 
				'slug' : "orders", 
				kenteken : ""+newObject.kenteken+"", 
				reparateur : ""+params.user+"", 
				opmerking : ""+newObject.opmerking+"",
				img : ""+newObject.img+"",
				checklist: ""+$scope.checklist+"",
				lat: ""+lat+"",
				lon: ""+lon+""
				}),  // pass in data as strings
				
				isArray: true,
				callback: ''
		  }).success(function(data) {
				
				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log('error');
				} else {
				  // if successful, bind success message to message
							
							ons.notification.alert({
								messageHTML: '<div>Order toegevoegd!</div>',
								// or messageHTML: '<div>Message in HTML</div>',
								title: data.kenteken,
								buttonLabel: 'OK',
								cancelable: true,
								animation: 'default', // or 'none'
								// modifier: 'optional-modifier'
								callback: function() {
									menu.setMainPage('home.html', {closeMenu: true})
								}
							});
				}
				
			  });
		}
	
	}
	
	$scope.submit = function () {
		
		var img = document.getElementById('theimg').getElementsByTagName('img')[0].src;
		console.log(img);
		
		var createdorder = new order($scope.kenteken,$scope.opmerking,$scope.reparateur,img);
		
		if ( getNum( localStorageService.get('orderCount') ) == null ){
			localStorageService.set('orderCount', 0);
		}
		
		var orderSize = parseInt(localStorageService.get('orderCount')) + 1;
		
		commitToStorage(orderSize,createdorder);
	}

    $scope.$watch(function(){
      return localStorageService.get('kenteken');
    }, function(value){
      $scope.kenteken = value;
    });

    $scope.clearAll = localStorageService.clearAll;
  }
);



ToolApp.controller('OrderController', function($http, $parse, $scope, localStorageService, $sce, $rootScope) {



	$scope.removeOrder = function(id) { //Remove for home view
	
			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?removeorder='+ id +'',
			   method:"GET"
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else {
				  // if successful, bind success message to message and fill the list
				
				$('.alert-dialog').remove(); 
				$('.alert-dialog-mask').remove(); 
				
				ons.notification.alert({
					messageHTML: '<div>'+data+'</div>',
					// or messageHTML: '<div>Message in HTML</div>',
					title: event.title,
					buttonLabel: 'OK',
					cancelable: true,
					animation: 'default', // or 'none'
					// modifier: 'optional-modifier'
					callback: function() {
						menu.setMainPage('home.html', {closeMenu: true})
					}	
				});
			}
			});
	};
	
	//console.log(params.orderkey);
	$scope.checklist = []; //Set up check list param
	if (is.set(params.orderkey)){	//Check if the param is set	
	
	$scope.orderkey = params.orderkey;



	
		//console.log (""+options+""); //Debug info
		
		options = params.orderkey; //Retrieve the orderkey
		get = localStorageService.get(options); //Get key
		parse = JSON.parse(get); //Parse the key values since it is JSON
		srcimg = document.getElementById('srcimg'); //Set img id
		
		if(options != null){ //Check if the key is not null

			if(offline == true){ //If offline load the local storage stuff..
			//Fill the data and unhide the img
			$scope.kenteken = parse.kenteken;
			$scope.opmerking = parse.opmerking;
			$scope.naam = parse.repa;
			$scope.img = parse.img;
			srcimg.style.display = 'block';
			
			} else { //We are online get ajax data

			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?singleorder='+ options +'',
			   method:"GET"
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else {
				  // if successful, bind success message to message and fill the list
				

						$scope.checklist = data.checklist.split(','); //Load checklist
						$scope.kenteken = data.kenteken;//Load kenteken
						$scope.opmerking = data.opmerking; //....
						$scope.naam = data.reparateur;
						$scope.img = data.img;
						srcimg.style.display = 'block';
						
						console.log($scope.checklist);
						if(is.set(data.lat) && data.lat != 0){ //Set location for google
							latlong = new google.maps.LatLng(data.lat, data.lon);
						}else{
							latlong = new google.maps.LatLng(51.452287, 5.482072);
						}

						
						ons.ready(function() {
							
							//Set google maps
							function map(){
								console.log(latlong);
								var mapOptions = {
									center: latlong,
									zoom: 16,
									mapTypeId: google.maps.MapTypeId.ROADMAP
								}
								var map = new google.maps.Map(document.getElementById("geolocation"), mapOptions);
								google.maps.event.trigger(map, "resize");
							}
							setTimeout(map(), 2000);
							

						});
						
						$http({
						   url:'http://i281875.iris.fhict.nl/phonetool/data.php?kenteken='+ data.kenteken +'',
						   method:"GET"
						}).success(function(hbase) {

							if (!hbase) {
							  // if not successful, bind errors to error variables
							  console.log(hbase);
							  console.log('error');
							} else {
							  // if successful, bind success message to message and fill the div with car info
							 
									$scope.auto = $sce.trustAsHtml(hbase);
							 
							}
						});
			
				 
				}
			});
			
			}
			
		}
	};

});



ToolApp.controller('OrderEditCtrl', function($http, $parse, $scope, localStorageService, $sce, transformRequestAsFormPost) {


	function getNum(val)
	{
	   if (isNaN(val)) 
		 return null;
	   else
		 return val;
	}

	
	$scope.removeOrder = function(id) { //Remove for home view
	
			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?removeorder='+ id +'',
			   method:"GET"
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else {
				  // if successful, bind success message to message and fill the list
				
				$('.alert-dialog').remove(); 
				$('.alert-dialog-mask').remove(); 
				
				ons.notification.alert({
					messageHTML: '<div>'+data+'</div>',
					// or messageHTML: '<div>Message in HTML</div>',
					title: event.title,
					buttonLabel: 'OK',
					cancelable: true,
					animation: 'default', // or 'none'
					// modifier: 'optional-modifier'
					callback: function() {
						menu.setMainPage('home.html', {closeMenu: true})
					}	
				});
			}
			});
	};
	
	//console.log(params.orderkey);
	$scope.checklist = []; //Set up check list param
	
	$scope.removeItem = function($index) { //Functions to remove checklist item
		$scope.checklist.splice( $scope.checklist.indexOf(), $index )
		console.log($index);
	};
	
	$scope.addCheck = function($index){ //Add function
		$scope.checklist.push($scope.newCheck);
		console.log($scope.checklist);
	};
	
	
	if (is.set(params.orderkey)){	//Check if the param is set	
	
	$scope.orderkey = params.orderkey;
	
		//console.log (""+options+""); //Debug info
		
		options = params.orderkey; //Retrieve the orderkey
		get = localStorageService.get(options); //Get key
		parse = JSON.parse(get); //Parse the key values since it is JSON
		srcimg = document.getElementById('srcimg'); //Set img id
		
		if(options != null){ //Check if the key is not null

			if(offline == true){ //If offline load the local storage stuff..
			//Fill the data and unhide the img
			$scope.kenteken = parse.kenteken;
			$scope.opmerking = parse.opmerking;
			$scope.naam = parse.repa;
			$scope.img = parse.img;
			srcimg.style.display = 'block';
			
			} else { //We are online get ajax data

			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?singleorder='+ options +'',
			   method:"GET"
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else {
				  // if successful, bind success message to message and fill the list
				 
						
						$scope.checklist = data.checklist.split(','); //Load checklist
						$scope.kenteken = data.kenteken;//Load kenteken
						$scope.opmerking = data.opmerking; //....
						$scope.naam = data.reparateur;
						$scope.img = data.img;
						srcimg.style.display = 'block';
						
				 
				}
			});
			
			}
			
		}
		
		
		//Save function
		
		function commitToStorage(objectCount,newObject) {
		
			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php',
			   method:"POST",
			   headers: {
				'X-Requested-With': 'XMLHttpRequest',
				'Content-Type': 'application/x-www-form-urlencoded'
			   },
			   transformRequest: transformRequestAsFormPost,
			    data    : eval({ 
				'slug' : "ordersupdate", 
				id: params.orderkey,
				kenteken : ""+newObject.kenteken+"", 
				reparateur : ""+params.user+"", 
				opmerking : ""+newObject.opmerking+"",
				img : ""+newObject.img+"",
				checklist: ""+$scope.checklist+""
				}),  // pass in data as strings
				
				isArray: true,
				callback: ''
		  }).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log('error');
				} else {
				  // if successful, bind success message to message
							console.log(data);
							ons.notification.alert({
								messageHTML: '<div>Order gewijzigd!</div>',
								// or messageHTML: '<div>Message in HTML</div>',
								title: data.kenteken,
								buttonLabel: 'OK',
								cancelable: true,
								animation: 'default', // or 'none'
								// modifier: 'optional-modifier'
								callback: function() {
									menu.setMainPage('home.html', {closeMenu: true})
								}
							});
				}
				
			  });
		}
	
	}
	
	$scope.submit = function () {
		
		var img = document.getElementById('theimg').getElementsByTagName('img')[0].src;
		console.log(img);
		
		var createdorder = new order($scope.kenteken,$scope.opmerking,$scope.reparateur,img);
		
		if ( getNum( localStorageService.get('orderCount') ) == null ){
			localStorageService.set('orderCount', 0);
		}
		
		var orderSize = parseInt(localStorageService.get('orderCount')) + 1;
		
		commitToStorage(orderSize,createdorder);
	}
		
		
		
		

});


ToolApp.controller('SettingsCtrl', function($http, $parse, $scope, localStorageService) {


	if (is.set(params.user)){//Check if the param is set	
	
		//console.log (""+options+""); //Debug info
		
		options = params.user; //Retrieve the orderkey
		get = localStorageService.get(options); //Get key
		parse = JSON.parse(get); //Parse the key values since it is JSON
		
		if(options != null){ //Check if the key is not null

			$http({
			   url:'http://i281875.iris.fhict.nl/phonetool/data.php?user='+ options +'',
			   method:"GET"
			}).success(function(data) {

				if (!data) {
				  // if not successful, bind errors to error variables
				  console.log(data);
				  console.log('error');
				} else {
				  // if successful, bind success message to message and fill the list
				 
						$scope.naam = data.naam;
						$scope.garage = data.garage;
						$scope.api_url = data.api_url;
				 
				}
			});
			
		}
	};

});

ToolApp.service('ControllerChecker', ['$controller', function($controller) {
    return {
        exists: function(controllerName) {
            if(typeof window[controllerName] == 'function') {
                return true;
            }
            try {
                $controller(controllerName);
                return true;
            } catch (error) {
                return !(error instanceof TypeError);
            }
        }
    };
}]);




ToolApp.factory("transformRequestAsFormPost",  function() {

	// I prepare the request data for the form post.
	function transformRequest( data, getHeaders ) {

		var headers = getHeaders();

		headers[ "Content-type" ] = "application/x-www-form-urlencoded; charset=utf-8";

		return( serializeData( data ) );

	}


	// Return the factory value.
	return( transformRequest );


	function serializeData( data ) {

			// If this is not an object, defer to native stringification.
			if ( ! angular.isObject( data ) ) {

				return( ( data == null ) ? "" : data.toString() );

			}

			var buffer = [];

			// Serialize each key in the object.
			for ( var name in data ) {

				if ( ! data.hasOwnProperty( name ) ) {

					continue;

				}

				var value = data[ name ];

				buffer.push(
					encodeURIComponent( name ) +
					"=" +
					encodeURIComponent( ( value == null ) ? "" : value )
				);

			}

			// Serialize the buffer and clean it up for transportation.
			var source = buffer
				.join( "&" )
				.replace( /%20/g, "+" )
			;

			return( source );

		}

	}
);


