Automotive Workshop Tool
====

This document describes the minimum information of the mobile app wich is created with Onsen-UI, AngularJS, Bootstrap.

## Requirement

 * Node.js - [Install Node.js](http://nodejs.org)
 * Cordova - Install by `npm install cordova`

## Development Instructions

1. Install dependencies

    $ npm install

2. Install Gulp globally

    $ npm install -g gulp

3. Run `gulp serve` and run the web server

    $ gulp serve

You should see running app on browser and you can start to run the app in the browser and extend it.

### Directory Layout

    README.md     --> This file
    www/          --> Asset files for app
      index.html  --> App entry point
      js/
		index.js  --> App script
      styles/
		app.css   --> App styling
      lib/onsen/
        stylus/   --> Stylus files for onsen-css-components.css
        js/       --> JS files for Onsen UI
        css/      --> CSS files for Onsen UI
    platforms/    --> Cordova platform directory
    plugins/      --> Cordova plugin directory
    merges/       --> Cordova merge directory
    hooks/        --> Cordova hook directory

## How the app works

1.	The app is build with the Onsen UI framework.
2.	The app contains different controllers and fixes to use with Windows Phone 10 (IE Cache for the ajax calls, http headers).
3.	The code is inline commented (javascript).

## What the app does
The app is a planning workshop tool that could be used by a car mechanic. It replaces paper work and made setting up a repair order a lot easier.
Possible things to do:
Create repair order, add pictures of the repair, view order with car information (based on the licenseplate), edit orders.

## Sensors and mobile dev
The camera sensor has been used to create the photos, 
The gps sensor is used to set the location of the order (this is usefull for customer data or if the car is picked up somewhere > like a damaged vehicle),
Also some hotfixes for the WIN 10 platform are included (like ie cache fix and the directory fix).

